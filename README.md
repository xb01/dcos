
# DC-OS (dcos) training repo

This repo contains training material from one day workshop conducted by
mesosphere. The student material, powerpoint slides, tools directory which
has ansible playbooks to deploy a DC/OS cluster running the community 
version of DC/OS.

## This repo url
https://gitlab.com/xb01/dcos

## static web content for accessing the lab material
http://xb01.gitlab.io/dcos/dcos-kickstart/index.html

## slide deck
http://xb01.gitlab.io/dcos/slides/DC-OS_1_10_KickStart.pptx
